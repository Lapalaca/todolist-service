# todolist-service

```shell
cd todolist-service
./gradlew clean bootRun
```

打开浏览器访问地址 `http://localhost:8080/heartbeat`
看到 `hello world!`

# API Doc

## Show todos

method: 'get'

url: 'http://localhost:8080/todos'

headers: { }

return example:

```json
[
    {
        "id": "111",
        "category": "Life",
        "content": "Work out",
        "createdAt": "2022-05-13T10:11:30"
    },
    {
        "id": "222",
        "category": "Study",
        "content": "Math Cha1",
        "createdAt": "2022-05-13T15:00:00"
    },
    {
        "id": "333",
        "category": "Life",
        "content": "Go Shopping",
        "createdAt": "2022-05-13T19:00:00"
    }
]
```

## Add todo

request example:
```java
var data = JSON.stringify({
  "category": "Test",
  "content": "C1",
  "createdAt": "2022-06-13T15:00:00"
});

var config = {
  method: 'post',
  url: 'http://localhost:8080/todos',
  headers: { 
    'Content-Type': 'application/json'
  },
  data : data
};
```

return example:

```json
[
    {
        "id": "111",
        "category": "Life",
        "content": "Work out",
        "createdAt": "2022-05-13T10:11:30"
    },
    {
        "id": "222",
        "category": "Study",
        "content": "Math Cha1",
        "createdAt": "2022-05-13T15:00:00"
    },
    {
        "id": "333",
        "category": "Life",
        "content": "Go Shopping",
        "createdAt": "2022-05-13T19:00:00"
    },
    {
        "id": "1654761018271",
        "category": "Test",
        "content": "C1",
        "createdAt": "2022-06-13T15:00:00"
    }
]
```

### Delete todo

request example:

```java
var data = JSON.stringify({
  "category": "Test",
  "content": "C1",
  "createdAt": "2022-06-13T15:00:00"
});

var config = {
  method: 'delete',
  url: 'http://localhost:8080/todos/111',
  headers: { 
    'Content-Type': 'application/json'
  },
  data : data
};
```

return example:
```json
[
    {
        "id": "222",
        "category": "Study",
        "content": "Math Cha1",
        "createdAt": "2022-05-13T15:00:00"
    },
    {
        "id": "333",
        "category": "Life",
        "content": "Go Shopping",
        "createdAt": "2022-05-13T19:00:00"
    }
]
```

### Update todo

request example:
```java
var data = JSON.stringify({
  "category": "Test",
  "content": "Update here"
});

var config = {
  method: 'put',
  url: 'http://localhost:8080/todos/111',
  headers: { 
    'Content-Type': 'application/json'
  },
  data : data
};
```

return example:
```json

```