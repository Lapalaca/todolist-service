package com.tw.todolistservice.controller;

import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RestController
public class TodolistController {

    static List<Todo> todos = new ArrayList<Todo>();

    public TodolistController() {
        Todo todo1 = new Todo("111", "Life", "Work out", LocalDateTime.parse("2022-05-13T10:11:30"));
        Todo todo2 = new Todo("222", "Study", "Math Cha1", LocalDateTime.parse("2022-05-13T15:00:00"));
        Todo todo3 = new Todo("333", "Life", "Go Shopping", LocalDateTime.parse("2022-05-13T19:00:00"));
        todos.add(todo1);
        todos.add(todo2);
        todos.add(todo3);
    }

    @GetMapping(value = "/heartbeat")
    public ResponseEntity<String> postTodo() {
        return ResponseEntity.ok().body("hello world!");
    }

    @GetMapping(value = "/todos")
    public ResponseEntity<List> getTodos() {
        return new ResponseEntity<List>(todos, HttpStatus.OK);
    }

    @PostMapping(value = "/todos", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List> addTodos(@RequestBody JSONObject msg) {
        String category = (String) msg.get("category");
        String content = (String) msg.get("content");
        String createdAt = (String) msg.get("createdAt");
        String id = String.valueOf(System.currentTimeMillis());
        Todo todo = new Todo(id, category, content, LocalDateTime.parse(createdAt));
        todos.add(todo);
        return new ResponseEntity<List>(todos, HttpStatus.OK);
    }

    @DeleteMapping(value = "/todos/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List> deleteTodos(@PathVariable String id) {
        todos.removeIf(item -> item.getId().equals(id));
        return new ResponseEntity<List>(todos, HttpStatus.OK);
    }

    @PutMapping(value = "/todos/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List> updateTodos(@RequestBody JSONObject msg, @PathVariable String id) {
        String category = (String) msg.get("category");
        String content = (String) msg.get("content");

        for (Todo item : todos) {
            if (item.getId().equals(id)) {
                item.setCategory(category);
                item.setContent(content);
                break;
            }
        }

        return new ResponseEntity<List>(todos, HttpStatus.OK);
    }

    public static class Todo {

        public String id;
        public String category;
        public String content;
        public LocalDateTime createdAt;

        public Todo(String id, String category, String content, LocalDateTime createdAt) {
            this.id = id;
            this.category = category;
            this.content = content;
            this.createdAt = createdAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public LocalDateTime getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
        }
    }
}
